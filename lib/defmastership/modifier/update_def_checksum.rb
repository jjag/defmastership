# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require 'defmastership/modifier/update_def'

module Defmastership
  module Modifier
    # modify one line after another
    class UpdateDefChecksum < UpdateDef
      private

      def reference_replacement(reference, match)
        "#{reference}(#{match[:explicit_version]}#{document.ref_to_def(reference).sha256_short})"
      end
    end
  end
end
