# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

module Defmastership
  # Apply modications on a list of related asciidoc sources
  class BatchModifier
    # Stores the overall invocation configuration
    # @return [YAML] configuration as eventualy modified by modifiers
    attr_reader :config
    # Stores the list of asciidoc files to modify and to save
    # @return [Hash{String => String}] asciidoc sources as modified by modifiers
    #  * :key filename
    #  * :value file content
    attr_reader :adoc_sources
    # Provides the list of performed modifications
    # @return [Array<Array<String>>] List of performed modifications
    # (each line: [Modifier, Was, Becomes])
    attr_reader :changes

    # @param config [YAML] the overall modifications configurations
    # @param adoc_sources [Hash{String => String}] asciidoctor files to be modified
    #  * Key: filename
    #  * Value: file content
    def initialize(config, adoc_sources)
      @config = config
      @adoc_sources = adoc_sources
      @changes = []
    end

    # Apply modications on asciidoc sources of @adoc_sources
    #
    # @param modifs [Array<String>] The modifications to apply
    def apply(modifs)
      modifs.each do |modif|
        modifier = modifier_from(modif)
        @adoc_sources = modifier.do_modifications(adoc_sources)
        config.fetch(modif)[:config] = modifier.config

        collect_changes(modifier, modif)
      end
    end

    private

    def modifier_from(modif)
      config_modif_sym = config[modif]
      raise(ArgumentError, "#{modif} is not a known modification") unless config_modif_sym

      Modifier::Factory.from_config(config_modif_sym)
    end

    def collect_changes(modifier, modif)
      modifier.changes.reduce(changes) do |acc, change|
        acc << ([modif.to_s] + change)
      end
    end
  end
end
