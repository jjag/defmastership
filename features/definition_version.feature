Feature: definitions version
  As a Responsible of definitions for a given document
  In order to detect handle versions definitions
  I want defmastership to export explicit versions in definitions

  Scenario: Extract one definition with explicit version to CSV
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(pouet)]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export thedoc.adoc`
    Then the file "thedoc.csv" should contain:
    """
    Type,Reference,Value,Checksum,Version
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde,pouet
    """
    And the stdout should not contain anything

  Scenario: Set initial explicit version when definition has changed
    Given a file named "ref_doc.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    initial text.
    --
    """
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever)]
    --
    modified text.
    --
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: requirement
        :first_version: a
        :ref_document: ./ref_doc.adoc
    """
    When I successfully run `defmastership modify --modifications update_requirement_version thedoc.adoc`
    And the stdout should not contain anything
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(a)]
    --
    modified text.
    --
    """

  Scenario: Set initial explicit version when definition summary has changed
    Given a file named "ref_doc.adoc" with:
    """
    [define, requirement, TOTO-0001, a nice summary]
    --
    Some text.
    --
    """
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever), a really nice summary]
    --
    Some text.
    --
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: requirement
        :first_version: a
        :ref_document: ./ref_doc.adoc
    """
    When I successfully run `defmastership modify --modifications update_requirement_version thedoc.adoc`
    And the stdout should not contain anything
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(a), a really nice summary]
    --
    Some text.
    --
    """

  Scenario: Set initial explicit version when definition has changed with multiple ref_files
    Given a file named "ref_doc1.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    initial text 1.
    --
    """
    Given a file named "ref_doc2.adoc" with:
    """
    [define, requirement, TOTO-0002(z)]
    --
    initial text 2.
    --
    """
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever)]
    --
    modified text.
    --

    [define, requirement, TOTO-0002(whatever)]
    --
    modified text again.
    --
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: requirement
        :first_version: a
        :ref_document: ['./ref_doc1.adoc', './ref_doc2.adoc']
    """
    When I successfully run `defmastership modify --modifications update_requirement_version thedoc.adoc`
    And the stdout should not contain anything
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(a)]
    --
    modified text.
    --

    [define, requirement, TOTO-0002(aa)]
    --
    modified text again.
    --
    """

  Scenario: Update explicit version based on git tag
    Given I initialize a git repo
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    initial text.
    --
    """
    And I add and commit the "thedoc.adoc" file
    And I set the "THE_TAG" tag
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever)]
    --
    modified text.
    --
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: requirement
        :first_version: a
        :ref_tag: 'THE_TAG'
    """
    When I successfully run `defmastership modify --modifications update_requirement_version thedoc.adoc`
    And the stdout should not contain anything
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(a)]
    --
    modified text.
    --
    """

  Scenario: Update explicit version based on git tag on a different git repo
    And a file named "defmastership-example.adoc" with:
    """
    [define, whatever, WHATEVER-0001]
    Simplest ever definition. !modified!
    With a single paragraph.
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: whatever
        :first_version: a
        :ref_tag: 'TEST_UPDATE_VERSION'
        :ref_repo: 'https://gitlab.com/defmastership/defmastership-example.git'
    """
    When I successfully run `defmastership modify --modifications update_requirement_version defmastership-example.adoc`
    And the stdout should not contain anything
    Then the file "defmastership-example.adoc" should contain:
    """
    [define, whatever, WHATEVER-0001(a)]
    Simplest ever definition. !modified!
    With a single paragraph.
    """

  Scenario: Update explicit version based on git tag and different filename
    Given I initialize a git repo
    And a file named "ref_doc.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    initial text.
    --
    """
    And I add and commit the "ref_doc.adoc" file
    And I set the "THE_TAG" tag
    And I remove the file "ref_doc.adoc"
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever)]
    --
    modified text.
    --
    """
    And a file named "ref_doc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever)]
    --
    modified text.
    --
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: requirement
        :first_version: a
        :ref_document: ./ref_doc.adoc
        :ref_tag: 'THE_TAG'
    """
    When I successfully run `defmastership modify --modifications update_requirement_version thedoc.adoc`
    And the stdout should not contain anything
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(a)]
    --
    modified text.
    --
    """

  Scenario: Do not set initial explicit version when definition has NOT changed 
    Given a file named "ref_doc.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    initial text.
    --
    """
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever)]
    --
    initial text.
    --
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: requirement
        :first_version: a
        :ref_document: ./ref_doc.adoc
    """
    When I successfully run `defmastership modify --modifications update_requirement_version thedoc.adoc`
    And the stdout should not contain anything
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001]
    --
    initial text.
    --
    """

  Scenario: Set initial explicit version when definition has changed and ref is not an initial version
    Given a file named "ref_doc.adoc" with:
    """
    [define, requirement, TOTO-0001(3)]
    --
    initial text.
    --
    """
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever)]
    --
    modified text.
    --
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: requirement
        :first_version: 1
        :ref_document: ./ref_doc.adoc
    """
    When I successfully run `defmastership modify --modifications update_requirement_version thedoc.adoc`
    And the stdout should not contain anything
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(4)]
    --
    modified text.
    --
    """

  Scenario: No initial explicit version when definition is new
    Given a file named "ref_doc.adoc" with:
    """
    [define, requirement, TOTO-0002]
    --
    initial text.
    --
    """
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever)]
    --
    modified text.
    --
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: requirement
        :first_version: 1
        :ref_document: ./ref_doc.adoc
    """
    When I successfully run `defmastership modify --modifications update_requirement_version thedoc.adoc`
    And the stdout should not contain anything
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001]
    --
    modified text.
    --
    """

  Scenario: update explicit version whith explicit checksum
    Given a file named "ref_doc.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    initial text.
    --
    """
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(whatever~abcd1234)]
    --
    modified text.
    --
    """
    And a file named "modifications.yml" with:
    """
    ---
    :update_requirement_version:
      :type: update_def_version
      :config:
        :def_type: requirement
        :first_version: a
        :ref_document: ./ref_doc.adoc
    """
    When I successfully run `defmastership modify --modifications update_requirement_version thedoc.adoc`
    And the stdout should not contain anything
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(a~abcd1234)]
    --
    modified text.
    --
    """

