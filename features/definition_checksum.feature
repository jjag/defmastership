Feature: definition checksum 
  As a Responsible of definitions for a given document
  In order to detect modifications of defintiions
  I want defmastership to export and update checksum in definitions

  Scenario: Extract one definition with WRONG explicit checksum to CSV 
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(~babe1234)]
    def one
    """
    When I run `defmastership export thedoc.adoc`
    Then it should fail with:
    """
    warning: TOTO-0001 has a wrong explicit checksum (should be ~d27cb5c3)
    """
    And the file "thedoc.csv" should contain:
    """
    Type,Reference,Value,Checksum,Wrong explicit checksum
    requirement,TOTO-0001,def one,~d27cb5c3,~babe1234
    """
    And the stdout should not contain anything

  Scenario: Extract two definitions with one WRONG explicit checksum to CSV 
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(~d27cb5c3)]
    def one

    [define, requirement, TOTO-0002(~babe1234)]
    def two
    """
    When I run `defmastership export thedoc.adoc`
    Then it should fail with:
    """
    warning: TOTO-0002 has a wrong explicit checksum (should be ~b80e1be5)
    """
    Then the file "thedoc.csv" should contain:
    """
    Type,Reference,Value,Checksum,Wrong explicit checksum
    requirement,TOTO-0001,def one,~d27cb5c3,\"\"
    requirement,TOTO-0002,def two,~b80e1be5,~babe1234
    """
    And the stdout should not contain anything

  Scenario: Export not failing even if WRONG explicit checksum 
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(~d27cb5c3)]
    def one

    [define, requirement, TOTO-0002(~babe1234)]
    def two
    """
    When I successfully run `defmastership export --no-fail thedoc.adoc`
    Then the file "thedoc.csv" should contain:
    """
    Type,Reference,Value,Checksum,Wrong explicit checksum
    requirement,TOTO-0001,def one,~d27cb5c3,\"\"
    requirement,TOTO-0002,def two,~b80e1be5,~babe1234
    """
    And the stdout should not contain anything
    And the stderr should contain:
    """
    warning: TOTO-0002 has a wrong explicit checksum (should be ~b80e1be5)
    """

  Scenario: Extract one definition with explicit checksum to CSV 
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001(~b86dcbde)]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export thedoc.adoc`
    Then the file "thedoc.csv" should contain:
    """
    Type,Reference,Value,Checksum
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde
    """
    And the stdout should not contain anything

  Scenario: Change one modifier
    Given a file named "modifications.yml" with:
    """
    ---
    :update_requirement_checksum:
      :type: update_def_checksum
      :config:
        :def_type: requirement
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    --
    the requirement value.
    --
    """
    When I successfully run `defmastership modify --modifications update_requirement_checksum thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-TEMP-XXX1(~244eed18)]
    --
    the requirement value.
    --
    """

  Scenario: Change one modifier with already set checksum
    Given a file named "modifications.yml" with:
    """
    ---
    :update_requirement_checksum:
      :type: update_def_checksum
      :config:
        :def_type: requirement
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1(~abcd1234)]
    --
    the requirement value.
    --
    """
    When I successfully run `defmastership modify --modifications update_requirement_checksum thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-TEMP-XXX1(~244eed18)]
    --
    the requirement value.
    --
    """

  Scenario: Checksum is calculated on included file for export
    Given a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    include::included.txt[]
    --
    """
    And a file named "included.txt" with:
    """
    Exemple of multiline requirement.
    Second line.
    """
    When I successfully run `defmastership export thedoc.adoc`
    Then the file "thedoc.csv" should contain:
    """
    Type,Reference,Value,Checksum
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde
    """
    And the stdout should not contain anything

  Scenario: Checksum is calculated on included file in ref modification
    Given a file named "modifications.yml" with:
    """
    ---
    :update_requirement_checksum:
      :type: update_def_checksum
      :config:
        :def_type: requirement
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    include::included.txt[]
    --
    """
    And a file named "included.txt" with:
    """
    Exemple of multiline requirement.
    Second line.
    """
    When I successfully run `defmastership modify --modifications update_requirement_checksum thedoc.adoc`
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(~b86dcbde)]
    --
    include::included.txt[]
    --
    """
    And the stdout should not contain anything

  Scenario: Checksum take into account variables in CSV export
    Given a file named "thedoc.adoc" with:
    """
    :variable: multiline
    [define, requirement, TOTO-0001]
    --
    Exemple of {variable} requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export thedoc.adoc`
    Then the file "thedoc.csv" should contain:
    """
    Type,Reference,Value,Checksum
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde
    """
    And the stdout should not contain anything

  Scenario: Checksum takes into account variables in ref modification
    Given a file named "modifications.yml" with:
    """
    ---
    :update_requirement_checksum:
      :type: update_def_checksum
      :config:
        :def_type: requirement
    """
    And a file named "thedoc.adoc" with:
    """
    :variable: multiline
    [define, requirement, TOTO-0001]
    --
    Exemple of {variable} requirement.
    Second line.
    --
    """
    When I successfully run `defmastership modify --modifications update_requirement_checksum thedoc.adoc`
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(~b86dcbde)]
    --
    Exemple of {variable} requirement.
    Second line.
    --
    """
    And the stdout should not contain anything

  Scenario: Checksum takes into account definition summary in ref modification
    Given a file named "modifications.yml" with:
    """
    ---
    :update_requirement_checksum:
      :type: update_def_checksum
      :config:
        :def_type: requirement
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    A nice summary
    Exemple of multiline requirement.
    Second line.
    --

    [define, requirement, TOTO-0002, A nice summary]
    --
    Exemple of multiline requirement.
    Second line.
    --

    :a_variable: summary
    [define, requirement, TOTO-0003, A nice {a_variable}]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership modify --modifications update_requirement_checksum thedoc.adoc`
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(~f8092f4e)]
    --
    A nice summary
    Exemple of multiline requirement.
    Second line.
    --

    [define, requirement, TOTO-0002(~f8092f4e), A nice summary]
    --
    Exemple of multiline requirement.
    Second line.
    --

    :a_variable: summary
    [define, requirement, TOTO-0003(~f8092f4e), A nice {a_variable}]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    And the stdout should not contain anything

  Scenario: Checksum does not take into account bad variables definition
    Given a file named "modifications.yml" with:
    """
    ---
    :update_requirement_checksum:
      :type: update_def_checksum
      :config:
        :def_type: requirement
    """
    And a file named "thedoc.adoc" with:
    """
    :variable:multiline
    [define, requirement, TOTO-0001]
    --
    Exemple of {variable} requirement.
    Second line.
    --
    """
    When I successfully run `defmastership modify --modifications update_requirement_checksum thedoc.adoc`
    Then the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0001(~2e7dd73e)]
    --
    Exemple of {variable} requirement.
    Second line.
    --
    """
    And the stdout should not contain anything

  Scenario: Checksum keep explicit version in ref modification
    Given a file named "modifications.yml" with:
    """
    ---
    :update_requirement_checksum:
      :type: update_def_checksum
      :config:
        :def_type: requirement
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1(toto~abcd1234)]
    --
    the requirement value.
    --
    """
    When I successfully run `defmastership modify --modifications update_requirement_checksum thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-TEMP-XXX1(toto~244eed18)]
    --
    the requirement value.
    --
    """

  Scenario: Checksum keep explicit version in ref setting
    Given a file named "modifications.yml" with:
    """
    ---
    :update_requirement_checksum:
      :type: update_def_checksum
      :config:
        :def_type: requirement
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1(toto)]
    --
    the requirement value.
    --
    """
    When I successfully run `defmastership modify --modifications update_requirement_checksum thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-TEMP-XXX1(toto~244eed18)]
    --
    the requirement value.
    --
    """
