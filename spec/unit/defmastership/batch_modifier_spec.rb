# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/batch_modifier')

module Defmastership
  # Modifier example
  class Toto
    include Modifier
  end

  # Modifier example
  class TuTu
    include Modifier
  end
end

RSpec.describe(Defmastership::BatchModifier) do
  subject(:batchmodifier) do
    described_class.new(config, adoc_sources)
  end

  let(:config) do
    {
      modifier1: { type: 'toto', config: { p: 1 } },
      modifier2: { type: 'toto', config: { p: 'whatever1' } },
      modifier3: { type: 'tu_tu', config: { p1: 'whatever2', p2: 'whatever3' } }
    }
  end
  let(:adoc_sources) do
    {
      'file1.adoc' => 'some text',
      'file2.adoc' => 'another text'
    }
  end

  describe '.new' do
    it { is_expected.not_to(be_nil) }
    it { is_expected.to(have_attributes(config: config)) }

    it do
      expect(batchmodifier).to(have_attributes(adoc_sources: adoc_sources))
    end
  end

  describe '#apply' do
    context 'with only one modification' do
      let(:toto1) { instance_double(Defmastership::Modifier::Toto, 'toto1') }
      let(:adoc_sources_modified) do
        {
          'file1.adoc' => 'some text modified',
          'file2.adoc' => 'another text'
        }
      end

      let(:config_modified) do
        {
          modifier1: { type: 'toto', config: { p: 'modified_param' } },
          modifier2: { type: 'toto', config: { p: 'whatever1' } },
          modifier3: { type: 'tu_tu', config: { p1: 'whatever2', p2: 'whatever3' } }
        }
      end

      before do
        allow(Defmastership::Modifier::Toto).to(receive(:new).once.and_return(toto1))
        allow(toto1).to(receive(:do_modifications).once.and_return(adoc_sources_modified))
        allow(toto1).to(receive(:config).once.and_return(p: 'modified_param'))
        allow(toto1).to(receive(:changes).once.and_return([%w[from1 to1], %w[from2 to2]]))
        batchmodifier.apply(%i[modifier1])
      end

      it do
        expect(Defmastership::Modifier::Toto).to(have_received(:new).with(p: 1))
      end

      it { expect(toto1).to(have_received(:do_modifications).with(adoc_sources)) }
      it { is_expected.to(have_attributes(adoc_sources: adoc_sources_modified)) }
      it { expect(toto1).to(have_received(:config).with(no_args)) }

      it { is_expected.to(have_attributes(config: config_modified)) }
      it { is_expected.to(have_attributes(changes: [%w[modifier1 from1 to1], %w[modifier1 from2 to2]])) }
    end

    context 'with two modifications' do
      let(:toto2) { instance_double(Defmastership::Modifier::Toto, 'toto2') }
      let(:tutu3) { instance_double(Defmastership::Modifier::TuTu, 'tutu3') }

      let(:config_modified) do
        {
          modifier1: { type: 'toto', config: { p: 1 } },
          modifier2: { type: 'toto', config: :whatever },
          modifier3: { type: 'tu_tu', config: :pouet }
        }
      end

      before do
        allow(Defmastership::Modifier::Toto).to(receive(:new).once.and_return(toto2))
        allow(toto2).to(receive(:do_modifications).once.and_return(:adoc_sources_modified_mod2))
        allow(toto2).to(receive(:config).once.and_return(:whatever))
        allow(Defmastership::Modifier::TuTu).to(receive(:new).once.and_return(tutu3))
        allow(tutu3).to(receive(:do_modifications).once.and_return(:adoc_sources_modified_mod3))
        allow(tutu3).to(receive(:config).once.and_return(:pouet))
        allow(toto2).to(receive(:changes).once.and_return([%w[from1 to1]]))
        allow(tutu3).to(receive(:changes).once.and_return([%w[from2 to2]]))
        batchmodifier.apply(%i[modifier2 modifier3])
      end

      it { expect(Defmastership::Modifier::Toto).to(have_received(:new).with(p: 'whatever1')) }
      it { expect(Defmastership::Modifier::TuTu).to(have_received(:new).with(p1: 'whatever2', p2: 'whatever3')) }
      it { expect(toto2).to(have_received(:do_modifications).with(adoc_sources)) }
      it { expect(toto2).to(have_received(:config).with(no_args)) }
      it { expect(tutu3).to(have_received(:do_modifications).with(:adoc_sources_modified_mod2)) }
      it { expect(tutu3).to(have_received(:config).with(no_args)) }
      it { is_expected.to(have_attributes(adoc_sources: :adoc_sources_modified_mod3)) }
      it { is_expected.to(have_attributes(config: config_modified)) }
      it { is_expected.to(have_attributes(changes: [%w[modifier2 from1 to1], %w[modifier3 from2 to2]])) }
    end

    context 'with wrong modification' do
      it do
        expect { batchmodifier.apply(%i[wrong-modification]) }
          .to(
            raise_error(ArgumentError, 'wrong-modification is not a known modification')
          )
      end
    end
  end
end
