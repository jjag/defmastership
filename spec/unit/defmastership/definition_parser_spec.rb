# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/definition_parser')

RSpec.describe(Defmastership::DefinitionParser) do
  subject(:parser) { described_class.new(callback_object) }

  let(:callback_object) { double }

  before do
    allow(callback_object).to(receive(:add_new_definition).with(any_args))
    allow(callback_object).to(receive(:add_line).with(any_args))
  end

  describe 'transitions' do
    it { is_expected.to(have_state(:idle)) }

    it { is_expected.to(transition_from(:idle).to(:wait_content).on_event(:new_definition, :whatever)) }

    it { is_expected.to(transition_from(:wait_content).to(:in_block).on_event(:block_delimiter, :whatever)) }
    it { is_expected.to(transition_from(:in_block).to(:idle).on_event(:block_delimiter, :whatever)) }
    it { is_expected.to(transition_from(:idle).to(:idle).on_event(:block_delimiter, :whatever)) }
    it { is_expected.to(transition_from(:single_para).to(:idle).on_event(:block_delimiter, :whatever)) }

    it { is_expected.to(transition_from(:wait_content).to(:single_para).on_event(:new_line, :whatever)) }
    it { is_expected.to(transition_from(:single_para).to(:single_para).on_event(:new_line, :whatever)) }
    it { is_expected.to(transition_from(:in_block).to(:in_block).on_event(:new_line, :whatever)) }
    it { is_expected.to(transition_from(:idle).to(:idle).on_event(:new_line, :whatever)) }

    it { is_expected.to(transition_from(:wait_content).to(:idle).on_event(:empty_line, :whatever)) }
    it { is_expected.to(transition_from(:single_para).to(:idle).on_event(:empty_line, :whatever)) }
    it { is_expected.to(transition_from(:idle).to(:idle).on_event(:empty_line, :whatever)) }
    it { is_expected.to(transition_from(:in_block).to(:in_block).on_event(:empty_line, :whatever)) }
  end

  describe 'actions' do
    it do
      parser.new_definition(:whatever)
      expect(callback_object).to(have_received(:add_new_definition).with(:whatever))
    end

    it do
      parser.new_definition(:whatever)
      parser.new_line(:other_whatever)
      expect(callback_object).to(have_received(:add_line).with(:other_whatever))
    end

    it do
      parser.new_definition(:whatever)
      parser.new_line(:other_whatever)
      parser.new_line(:other_whatever2)
      expect(callback_object).to(have_received(:add_line).with(:other_whatever2))
    end

    it do
      parser.new_definition(:whatever)
      parser.block_delimiter(:other_whatever)
      parser.new_line(:other_whatever2)
      expect(callback_object).to(have_received(:add_line).with(:other_whatever2))
    end
  end
end
