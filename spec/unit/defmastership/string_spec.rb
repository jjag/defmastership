# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/comment_filter')

RSpec.describe(String) do
  context 'when .commented?' do
    it { expect('blabla'.commented?).to(be(false)) }
    it { expect('//blabla'.commented?).to(be(true)) }
    it { expect('///blabla'.commented?).to(be(false)) }
  end
end
