# Copyright (c) 2021 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/modifier/rename_included_files')

RSpec.describe(Defmastership::Modifier::RenameIncludedFiles) do
  subject(:includeschanger) { described_class.new({}) }

  describe '.new' do
    it { expect(described_class.ancestors).to(include(Defmastership::Modifier::ModifierCommon)) }
    it { is_expected.not_to(be_nil) }
    it { is_expected.to(have_attributes(from_regexp: '')) }
    it { is_expected.to(have_attributes(to_template: '')) }
  end

  describe '.replacement_methods' do
    it { expect(described_class.replacement_methods).to(eq(%i[replace])) }
  end

  describe '.respond_to_missing?' do
    it { expect(includeschanger.respond_to?(:add_new_definition)).to(be(true)) }
    it { expect(includeschanger.respond_to?(:not_implemented_random_method)).to(be(false)) }
  end

  describe '#replace' do
    context 'when NOT valid include' do
      subject(:includeschanger) do
        described_class.new(
          from_regexp: 'orig',
          to_template: 'dest'
        )
      end

      before do
        allow(File).to(receive(:rename))
      end

      context 'when the include statement do not match' do
        before do
          ['[define,requirement,REFERENCE]', '--'].each do |line|
            includeschanger.replace(line)
          end
        end

        it do
          expect(includeschanger.replace('badinclude::orig[]'))
            .to(eq('badinclude::orig[]'))
        end

        it do
          includeschanger.replace('badinclude::orig[]')
          expect(includeschanger).to(have_attributes(changes: []))
        end
      end

      context 'when the include is not in a definition' do
        before do
          ['[define,requirement,REFERENCE]', '--', 'text', '--'].each do |line|
            includeschanger.replace(line)
          end
        end

        it do
          expect(includeschanger.replace('include::orig[]'))
            .to(eq('include::orig[]'))
        end

        it do
          includeschanger.replace('include::orig[]')
          expect(includeschanger).to(have_attributes(changes: []))
        end
      end

      context 'when the line is commented' do
        before do
          includeschanger.replace('[define,requirement,REFERENCE]')
          includeschanger.replace('--')
        end

        it do
          expect(includeschanger.replace('// include::orig[]'))
            .to(eq('// include::orig[]'))
        end
      end

      context 'when the cancel regexp is met' do
        subject(:includeschanger) do
          described_class.new(
            from_regexp: 'orig',
            cancel_if_match: 'orig',
            to_template: 'dest'
          )
        end

        before do
          includeschanger.replace('[define,requirement,REFERENCE]')
          includeschanger.replace('--')
        end

        it do
          expect(includeschanger.replace("include::orig[]\n"))
            .to(eq("include::orig[]\n"))
        end
      end
    end

    context 'when valid include' do
      before do
        allow(File).to(receive(:rename))
      end

      context 'when really simple rule' do
        subject(:includeschanger) do
          described_class.new(
            from_regexp: 'orig',
            cancel_if_match: '_no_cancel_',
            to_template: 'dest'
          )
        end

        before do
          includeschanger.replace('[define,requirement,REFERENCE]')
          includeschanger.replace('--')
        end

        it do
          expect(includeschanger.replace("include::orig[]\n"))
            .to(eq("include::dest[]\n"))
        end

        it do
          includeschanger.replace("include::orig[]\n")
          expect(File).to(have_received(:rename).with('orig', 'dest'))
        end

        it do
          expect(includeschanger.replace("include::orig[leveloffset=offset,lines=ranges]\n"))
            .to(eq("include::dest[leveloffset=offset,lines=ranges]\n"))
        end

        it do
          includeschanger.replace("include::orig[leveloffset=offset,lines=ranges]\n")
          expect(File).to(have_received(:rename).with('orig', 'dest'))
        end

        it do
          includeschanger.replace("include::orig[]\n")
          expect(includeschanger).to(have_attributes(changes: [%w[orig dest]]))
        end

        it do
          expect(includeschanger.replace("include::toto/orig[]\n"))
            .to(eq("include::toto/dest[]\n"))
        end

        it do
          includeschanger.replace("include::toto/orig[]\n")
          expect(File).to(have_received(:rename).with('toto/orig', 'toto/dest'))
        end

        it do
          includeschanger.replace("include::toto/orig[]\n")
          expect(includeschanger).to(have_attributes(changes: [%w[toto/orig toto/dest]]))
        end
      end

      context 'when complex from_regexp' do
        subject(:includeschanger) do
          described_class.new(
            from_regexp: '(?<origin>.*\.extension)',
            to_template: '%<reference>s_%<origin>s'
          )
        end

        before do
          includeschanger.replace('[define,requirement,REF]')
        end

        it do
          expect(includeschanger.replace('include::any_path/one_file.extension[]'))
            .to(eq('include::any_path/REF_one_file.extension[]'))
        end

        it do
          includeschanger.replace('include::any_path/one_file.extension[]')
          expect(File).to(have_received(:rename).with('any_path/one_file.extension', 'any_path/REF_one_file.extension'))
        end

        it do
          changes = [%w[any_path/one_file.extension any_path/REF_one_file.extension]]
          includeschanger.replace('include::any_path/one_file.extension[]')
          expect(includeschanger).to(have_attributes(changes: changes))
        end
      end

      context 'when path with variable' do
        subject(:includeschanger) do
          described_class.new(
            from_regexp: 'orig',
            to_template: 'dest'
          )
        end

        before do
          includeschanger.replace(':any: one')
          includeschanger.replace(':path: two')
          includeschanger.replace(':var: three')
          includeschanger.replace('[define,requirement,REFERENCE]')
          includeschanger.replace('--')
        end

        it do
          expect(includeschanger.replace('include::{any}/orig[]'))
            .to(eq('include::{any}/dest[]'))
        end

        it do
          includeschanger.replace('include::{any}{var}/orig[]')
          expect(File).to(have_received(:rename).with('onethree/orig', 'onethree/dest'))
        end

        it do
          includeschanger.replace('include::{any}_{path}/orig[]')
          expect(File).to(have_received(:rename).with('one_two/orig', 'one_two/dest'))
        end

        it do
          expect { includeschanger.replace('include::{bad_variable}/orig[]') }
            .to(raise_error(KeyError, 'key not found: :bad_variable'))
        end

        it do
          includeschanger.replace('include::{any}_{path}/orig[]')
          expect(includeschanger).to(have_attributes(changes: [%w[one_two/orig one_two/dest]]))
        end
      end
    end
  end
end
