# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/modifier/update_def_version')
require('git')

RSpec.describe(Defmastership::Modifier::UpdateDefVersion) do
  subject(:modifier) { described_class.new({}) }

  describe '.new' do
    it { expect(described_class.ancestors).to(include(Defmastership::Modifier::ModifierCommon)) }
    it { is_expected.not_to(be_nil) }
    it { is_expected.to(have_attributes(def_type: '')) }
    it { is_expected.to(have_attributes(ref_document: [])) }
    it { is_expected.to(have_attributes(first_version: '')) }
  end

  describe '.replacement_methods' do
    it { expect(described_class.replacement_methods).to(eq(%i[replace_reference])) }
  end

  describe '#do_modifications' do
    context 'when only one ref document' do
      subject(:modifier) do
        described_class.new(
          ref_document: 'ref_doc.adoc',
          def_type: 'req',
          first_version: 'a'
        )
      end

      let(:document)       { instance_double(Defmastership::Document, 'document')          }
      let(:ref_document)   { instance_double(Defmastership::Document, 'ref_document')      }
      let(:definition)     { instance_double(Defmastership::Definition, 'definition')      }
      let(:ref_definition) { instance_double(Defmastership::Definition, 'ref_definitions') }
      let(:adoc_sources) do
        {
          'file1.adoc' => "[define,req,REFERENCE]\nfile1 line2",
          'file2.adoc' => "file2 line1\nfile2 line2"
        }
      end

      before do
        allow(Defmastership::Document).to(receive(:new).twice.and_return(ref_document, document))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file1.adoc'))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file2.adoc'))
        allow(ref_document).to(receive(:parse_file_with_preprocessor).with('ref_doc.adoc'))
        allow(document).to(receive(:ref_to_def).with('REFERENCE').and_return(definition))
        allow(ref_document).to(receive(:ref_to_def).with('REFERENCE').and_return(ref_definition))
        allow(ref_definition).to(receive(:explicit_version).with(no_args).and_return(nil))
        allow(definition).to(receive(:sha256_short).with(no_args).and_return('something'))
        allow(ref_definition).to(receive(:sha256_short).with(no_args).and_return('something_else'))

        modifier.do_modifications(adoc_sources)
      end

      it { is_expected.to(have_attributes(ref_document: ['ref_doc.adoc'])) }
      it { expect(Defmastership::Document).to(have_received(:new).twice) }
      it { expect(document).to(have_received(:parse_file_with_preprocessor).twice) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).with('ref_doc.adoc')) }

      it do
        expect(modifier.do_modifications(adoc_sources).fetch('file1.adoc')).to(include('REFERENCE(a)'))
      end
    end

    context 'when more ref document' do
      subject(:modifier) do
        described_class.new(
          ref_document: ['ref_doc1.adoc', 'ref_doc2.adoc'],
          def_type: 'req',
          first_version: 'a'
        )
      end

      let(:document)       { instance_double(Defmastership::Document, 'document')          }
      let(:ref_document)   { instance_double(Defmastership::Document, 'ref_document')      }
      let(:definition)     { instance_double(Defmastership::Definition, 'definition')      }
      let(:ref_definition) { instance_double(Defmastership::Definition, 'ref_definitions') }
      let(:adoc_sources) do
        {
          'file1.adoc' => "[define,req,REFERENCE]\nfile1 line2",
          'file2.adoc' => "file2 line1\nfile2 line2"
        }
      end

      before do
        allow(Defmastership::Document).to(receive(:new).twice.and_return(ref_document, document))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file1.adoc'))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file2.adoc'))
        allow(ref_document).to(receive(:parse_file_with_preprocessor).with('ref_doc1.adoc'))
        allow(ref_document).to(receive(:parse_file_with_preprocessor).with('ref_doc2.adoc'))
        allow(document).to(receive(:ref_to_def).with('REFERENCE').and_return(definition))
        allow(ref_document).to(receive(:ref_to_def).with('REFERENCE').and_return(ref_definition))
        allow(ref_definition).to(receive(:explicit_version).with(no_args).and_return(nil))
        allow(definition).to(receive(:sha256_short).with(no_args).and_return('something'))
        allow(ref_definition).to(receive(:sha256_short).with(no_args).and_return('something_else'))

        modifier.do_modifications(adoc_sources)
      end

      it { expect(Defmastership::Document).to(have_received(:new).twice) }
      it { expect(document).to(have_received(:parse_file_with_preprocessor).twice) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).with('ref_doc1.adoc')) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).with('ref_doc2.adoc')) }
    end

    context 'when ref_tag is provided' do
      subject(:modifier) do
        described_class.new(
          ref_tag: 'THE_TAG',
          def_type: 'req',
          first_version: 'a'
        )
      end

      let(:document)       { instance_double(Defmastership::Document, 'document')          }
      let(:ref_document)   { instance_double(Defmastership::Document, 'ref_document')      }
      let(:definition)     { instance_double(Defmastership::Definition, 'definition')      }
      let(:ref_definition) { instance_double(Defmastership::Definition, 'ref_definitions') }
      let(:adoc_sources) do
        {
          'file1.adoc' => "[define,req,REFERENCE]\nfile1 line2",
          'file2.adoc' => "file2 line1\nfile2 line2"
        }
      end

      before do
        allow(Dir).to(receive(:mktmpdir).and_yield('tmp'))
        allow(Git).to(receive(:clone))
        allow(Defmastership::Document).to(receive(:new).twice.and_return(ref_document, document))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file1.adoc'))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file2.adoc'))
        allow(ref_document).to(receive(:parse_file_with_preprocessor).with('tmp/file1.adoc'))
        allow(ref_document).to(receive(:parse_file_with_preprocessor).with('tmp/file2.adoc'))
        allow(document).to(receive(:ref_to_def).with('REFERENCE').and_return(definition))
        allow(ref_document).to(receive(:ref_to_def).with('REFERENCE').and_return(ref_definition))
        allow(ref_definition).to(receive(:explicit_version).with(no_args).and_return(nil))
        allow(definition).to(receive(:sha256_short).with(no_args).and_return('something'))
        allow(ref_definition).to(receive(:sha256_short).with(no_args).and_return('something_else'))

        modifier.do_modifications(adoc_sources)
      end

      it { expect(Dir).to(have_received(:mktmpdir).with('defmastership')) }
      it { expect(Git).to(have_received(:clone).with('.', 'tmp', branch: 'THE_TAG')) }
      it { expect(Defmastership::Document).to(have_received(:new).twice) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).with('tmp/file1.adoc')) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).with('tmp/file2.adoc')) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).twice) }

      it do
        expect(modifier.do_modifications(adoc_sources).fetch('file1.adoc')).to(include('REFERENCE(a)'))
      end
    end

    context 'when ref_tag and ref_repo is provided' do
      subject(:modifier) do
        described_class.new(
          ref_tag: 'THE_TAG',
          ref_repo: 'not dot',
          def_type: 'req',
          first_version: 'a'
        )
      end

      let(:document)       { instance_double(Defmastership::Document, 'document')          }
      let(:ref_document)   { instance_double(Defmastership::Document, 'ref_document')      }
      let(:definition)     { instance_double(Defmastership::Definition, 'definition')      }
      let(:ref_definition) { instance_double(Defmastership::Definition, 'ref_definitions') }
      let(:adoc_sources) do
        {
          'file1.adoc' => "[define,req,REFERENCE]\nfile1 line2",
          'file2.adoc' => "file2 line1\nfile2 line2"
        }
      end

      before do
        allow(Dir).to(receive(:mktmpdir).and_yield('tmp'))
        allow(Git).to(receive(:clone))
        allow(Defmastership::Document).to(receive(:new).twice.and_return(ref_document, document))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file1.adoc'))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file2.adoc'))
        allow(ref_document).to(receive(:parse_file_with_preprocessor).with('tmp/file1.adoc'))
        allow(ref_document).to(receive(:parse_file_with_preprocessor).with('tmp/file2.adoc'))
        allow(document).to(receive(:ref_to_def).with('REFERENCE').and_return(definition))
        allow(ref_document).to(receive(:ref_to_def).with('REFERENCE').and_return(ref_definition))
        allow(ref_definition).to(receive(:explicit_version).with(no_args).and_return(nil))
        allow(definition).to(receive(:sha256_short).with(no_args).and_return('something'))
        allow(ref_definition).to(receive(:sha256_short).with(no_args).and_return('something_else'))

        modifier.do_modifications(adoc_sources)
      end

      it { expect(Dir).to(have_received(:mktmpdir).with('defmastership')) }
      it { expect(Git).to(have_received(:clone).with('not dot', 'tmp', branch: 'THE_TAG')) }
      it { expect(Defmastership::Document).to(have_received(:new).twice) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).with('tmp/file1.adoc')) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).with('tmp/file2.adoc')) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).twice) }

      it do
        expect(modifier.do_modifications(adoc_sources).fetch('file1.adoc')).to(include('REFERENCE(a)'))
      end
    end

    context 'when both ref_tag and ref_document are provided' do
      subject(:modifier) do
        described_class.new(
          ref_tag: 'THE_TAG',
          ref_document: './another/doc.adoc',
          def_type: 'req',
          first_version: 'a'
        )
      end

      let(:document)       { instance_double(Defmastership::Document, 'document')          }
      let(:ref_document)   { instance_double(Defmastership::Document, 'ref_document')      }
      let(:definition)     { instance_double(Defmastership::Definition, 'definition')      }
      let(:ref_definition) { instance_double(Defmastership::Definition, 'ref_definitions') }
      let(:adoc_sources) do
        {
          'file1.adoc' => "[define,req,REFERENCE]\nfile1 line2",
          'file2.adoc' => "file2 line1\nfile2 line2"
        }
      end

      before do
        allow(Dir).to(receive(:mktmpdir).and_yield('tmp'))
        allow(Git).to(receive(:clone))
        allow(Defmastership::Document).to(receive(:new).twice.and_return(ref_document, document))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file1.adoc'))
        allow(document).to(receive(:parse_file_with_preprocessor).with('file2.adoc'))
        allow(ref_document).to(receive(:parse_file_with_preprocessor))
        allow(document).to(receive(:ref_to_def).with('REFERENCE').and_return(definition))
        allow(ref_document).to(receive(:ref_to_def).with('REFERENCE').and_return(ref_definition))
        allow(ref_definition).to(receive(:explicit_version).with(no_args).and_return(nil))
        allow(definition).to(receive(:sha256_short).with(no_args).and_return('something'))
        allow(ref_definition).to(receive(:sha256_short).with(no_args).and_return('something_else'))

        modifier.do_modifications(adoc_sources)
      end

      it { expect(Dir).to(have_received(:mktmpdir).with('defmastership')) }
      it { expect(Git).to(have_received(:clone).with('.', 'tmp', branch: 'THE_TAG')) }
      it { expect(Defmastership::Document).to(have_received(:new).twice) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).with('tmp/./another/doc.adoc')) }
      it { expect(ref_document).to(have_received(:parse_file_with_preprocessor).once) }

      it do
        expect(modifier.do_modifications(adoc_sources).fetch('file1.adoc')).to(include('REFERENCE(a)'))
      end
    end
  end

  describe '#replace_reference' do
    subject(:modifier) do
      described_class.new(
        def_type: 'requirement',
        first_version: 'a'
      )
    end

    let(:document)       { instance_double(Defmastership::Document, 'document')          }
    let(:ref_document)   { instance_double(Defmastership::Document, 'ref_document')      }
    let(:definition)     { instance_double(Defmastership::Definition, 'definition')      }
    let(:ref_definition) { instance_double(Defmastership::Definition, 'ref_definitions') }

    before do
      allow(File).to(receive(:rename))
      allow(Defmastership::Document).to(receive(:new).with(no_args).and_return(ref_document, document))
    end

    context 'when definition has not the good type' do
      it do
        expect(modifier.replace_reference('[define,req,REFERENCE]'))
          .to(eq('[define,req,REFERENCE]'))
      end
    end

    context 'when definition has the good type' do
      before do
        allow(document).to(receive(:ref_to_def).with('REFERENCE').and_return(definition))
        allow(definition).to(receive(:sha256_short).and_return('~abcd1234'))
      end

      context 'when definition has NOT changed' do
        before do
          allow(ref_document).to(receive(:ref_to_def).with('REFERENCE').and_return(ref_definition))
          allow(ref_definition).to(receive(:sha256_short).and_return('~abcd1234'))
        end

        it do
          allow(ref_definition).to(receive(:explicit_version).and_return(nil))
          expect(modifier.replace_reference('[define,requirement,REFERENCE]'))
            .to(eq('[define,requirement,REFERENCE]'))
        end

        it do
          allow(ref_definition).to(receive(:explicit_version).and_return('c'))
          expect(modifier.replace_reference('[define,requirement,REFERENCE]'))
            .to(eq('[define,requirement,REFERENCE(c)]'))
        end

        it do
          allow(ref_definition).to(receive(:explicit_version).and_return('c'))
          expect(modifier.replace_reference('[define,requirement,REFERENCE(tyty~1234)]'))
            .to(eq('[define,requirement,REFERENCE(c~1234)]'))
        end
      end

      context 'when definition has changed' do
        before do
          allow(ref_document).to(receive(:ref_to_def).with('REFERENCE').and_return(ref_definition))
          allow(ref_definition).to(receive(:sha256_short).and_return('~4321aaaa'))
        end

        [
          [nil,    '',            '(a)'],
          ['c',    '',            '(d)'],
          ['c',    '(tyty~1234)', '(d~1234)'],
          ['2',    '',            '(3)'],
          ['1222', '',            '(1223)'],
          ['abb',  '',            '(abc)']
        ].each do |ref, from, to|
          it do
            allow(ref_definition).to(receive(:explicit_version).and_return(ref))
            expect(modifier.replace_reference("[define,requirement,REFERENCE#{from}]"))
              .to(eq("[define,requirement,REFERENCE#{to}]"))
          end
        end
      end

      context 'when definition is new' do
        before do
          allow(ref_document).to(receive(:ref_to_def).with('REFERENCE').and_return(nil))
        end

        it do
          expect(modifier.replace_reference('[define,requirement,REFERENCE(whatever)]'))
            .to(eq('[define,requirement,REFERENCE]'))
        end

        it do
          expect(modifier.replace_reference('[define,requirement,REFERENCE(~1234)]'))
            .to(eq('[define,requirement,REFERENCE(~1234)]'))
        end
      end
    end
  end
end
