# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

RSpec.describe(Defmastership) do
  it 'has a version number' do
    expect(Defmastership::VERSION).not_to(be_nil)
  end
end
