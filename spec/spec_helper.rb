# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('aasm/rspec')
require('aruba/rspec')

# formatter = [SimpleCov::Formatter::HTMLFormatter]
# SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new(formatter)

require('simplecov')

SimpleCov.start do
  command_name 'spec:unit'

  add_group 'Libraries', 'lib'
  add_group 'Unit test', 'spec/unit'

  add_filter 'config'
  add_filter 'vendor'

  minimum_coverage 100

  enable_coverage :branch
end

RSpec.configure do |config|
  config.include(Aruba::Api)
end

RSpec::Matchers.define(:matchdata_including) do |h|
  match do |matchdata|
    h.all? do |key, _|
      matchdata[key] == h[key]
    end
  end
end

require('defmastership')
